﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {

        //always keep the main function clean. avoid writing any code other than calling functions and creating object instances
        static void Main(string[] args)
        {
            #region main code stuff.
            BasicInputAndOutput();

            //Four Types Of Functions 

            string somestring = "Bruce Wayne";
            //Send Paramters and Recieve return
            string response = SendAndRecieve(somestring);

            //Send Parameters and Received nothing
            SendOnly(somestring);
            //Send nothing and recieve return
            response = ReceiveOnly();

            //Send nothinig nad receive nothing. 
            NothingFunction();

            Console.ReadLine();

            #endregion

            #region Debugging
            NothingFunction();
            SendOnly(somestring);
            var x = ReceiveOnly();
            var y = SendAndRecieve(somestring);



            #endregion





        }

        #region functions with basics
        //this function takes nothing as parameters
        //returns nothing 
        private static void NothingFunction()
        {
            //this is nothing. 
            var dummy = "";
        }

        //this functions takes no parameters
        //but returns a simple string 
        private static string ReceiveOnly()
        {
            string someresponsestring = "Batman";
            return someresponsestring;
        }

        //this function receives a string
        // but does not return anything
        private static void SendOnly(string somestring)
        {
            //do nothing.
            var dummy = "";
        }

        //this function takes something and
        //also returns something. 
        private static string SendAndRecieve(string somestring)
        {
            string someresponsestring = "Batman";
            return someresponsestring;
        }

        //this a function that does some basic stuff
        private static void BasicInputAndOutput()
        {
            //create two string variables and assigning them some values. 
            var message = "hello world";
            var message2 = "Ameesha Patel is the greatest half decent actress in the world";
            //building the final output message. and also do some essential formatting so it looks good.
            var outputmessage = message + ".\n" + message2;
            //print the output message.
            Console.WriteLine(outputmessage);
            //put this so that the program will not exit and keep the output window. 
            //the moment any key is pressed, the program will exist. 

            //lets create three new variables
            int somenumber = 5;
            double somedoublenumber = 5.5;
            string somestring = "I'm going to make him an offer he can't refuse";
            outputmessage = "\nThis is the number - " + somenumber +
                            "\nThis is the floating value - " + somedoublenumber +
                            "\nThis is the sentence from string - " + somestring;
            //print the output message.
            Console.WriteLine(outputmessage);
        }

        #endregion
    }
}


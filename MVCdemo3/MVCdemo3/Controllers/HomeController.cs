﻿using MVCdemo3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Showlist()
        {
            ViewBag.Message = "This page shows a simple list";

            //lets build a list. 

            //create a collection
            var ListOfSuperHeroes = new List<SuperHero>();

            ListOfSuperHeroes = GetFiveSuperHeroes();


            //return View(db.NewsModel.ToList());
            return View(ListOfSuperHeroes);
        }


        //this creates a list of super heros. 
        //we will display this in the view
        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 50;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);


            return tempList;
        }
        public ActionResult List()
        {
            ViewBag.Message = "This page shows a simple list";

            //lets build a list. 

            //create a collection
            var ListOfFood = new List<FoodItems>();

            ListOfFood = GetFiveFoodItems();


            //return View(db.NewsModel.ToList());
            return View(ListOfFood);
        }
        private static List<FoodItems> GetFiveFoodItems()
        {
            //create a n empty list.
            var tempList = new List<FoodItems>();

            //five super heroes. 
            var name = "";
            var country = "";
            var price = 0;
            

            //five super heroes. 
            var tempHero = new FoodItems();
            var tempHero2 = new FoodItems();
            var tempHero3 = new FoodItems();
            var tempHero4 = new FoodItems();
            var tempHero5 = new FoodItems();
            var tempHero6 = new FoodItems();

            name = "Gulab Jamun";
            country = "India";
            price = 15;
           
            tempHero.name = name;
            tempHero.country = country;
            tempHero.price = price;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "Cheesecake";
            country = "Usa";
            price = 25;

            tempHero2.name = name;
            tempHero2.country = country;
            tempHero2.price = price;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "Gulab Jamun";
            country = "India";
            price = 15;

            tempHero3.name = name;
            tempHero3.country = country;

            tempHero3.price = price;
            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Gulab Jamun";
            country = "India";
            price = 15;

            tempHero4.name = name;
            tempHero4.country = country;

            tempHero4.price = price;
            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Gulab Jamun";
            country = "India";
            price = 15;

            tempHero5.name = name;
            tempHero5.country = country;

            tempHero5.price = price;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "Gulab Jamun";
            country = "India";
            price = 15;

            tempHero6.name = name;
            tempHero6.country = country;

            tempHero6.price = price;

            //add this hero to list. 
            tempList.Add(tempHero6);


            return tempList;
        }


    }


}
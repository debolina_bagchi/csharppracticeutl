﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCdemo3.Models
{
    public class FoodItems
    {
        public String name { get; set; }
        public String country { get; set; }
        public int price { get; set; }

    }
}
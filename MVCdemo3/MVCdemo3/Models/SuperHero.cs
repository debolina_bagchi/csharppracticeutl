﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCdemo3.Models
{
    public class SuperHero 
    {
        // GET: SuperHero
        
        public String name { get; set; }
        public String power { get; set; }
        public String brand { get; set; }
        public int age { get; set; }
            
        
    }
}
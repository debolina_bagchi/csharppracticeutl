﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestDemo
{
    class Utesting
    {
        ////unit test for code reuse.
        //copy paste this code file to build new unit tests quickly.
        [TestClass]
        public class SampleTest
        {
            [TestMethod]
            public void TestSample1()
            {
                var actual = true;
                var expected = true;
                Assert.AreEqual(expected, actual);

            }
            [TestMethod]
            public void TestSample2()
            {
                var actual = true;
                var expected =false;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSample3()
            {
                var input1 = 102388.256;
                var input2 = 653895.456;
                 
                var actual = 756283.712;
                var expected = Program.SumOfTwoNumber(input1,input2);
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSample4()
            {
                var input1 = 102388.256;
                var input2 = 653895.456;

                var actual = Program.SumOfTwoNumber(input1, input2);
                var expected = 756283.712;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSample5()
            {
                var input1 = 1088.587;
                var input2 = 2698.412;

                var actual = Program.SumOfTwoNumber(input1, input2);
                var expected = 3786.999;
                Assert.AreEqual(expected, actual);
            }
        }

    }
}

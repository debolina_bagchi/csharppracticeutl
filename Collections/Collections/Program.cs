﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Program
    {

        static void Main(string[] args)
        {
            var ListOfSuperHeros = new List<Superhero>();
            ListOfSuperHeros = GetFiveSuperHeroes();


            var ListOfSuperHero2 = new List<Superhero>();
            var temphero1 = new Superhero();
            temphero1.age = 20;
            temphero1.name = "Flash";
            temphero1.brand = "DC";
            temphero1.power = "speed";

            //var Superhero2 = new List< Superhero > ();
            var temphero2 = new Superhero();
            temphero2.age = 40;
            temphero2.name = "Batman";
            temphero2.brand = "DC";
            temphero2.power = "Money";

            ListOfSuperHero2.Add(temphero1);
            ListOfSuperHero2.Add(temphero2);
           

            var FinalList = new List<Superhero>();
            FinalList.AddRange(ListOfSuperHeros);
            FinalList.AddRange(ListOfSuperHeros);
             //ShowSuperHeroes(ListOfSuperHero2);
            //ShowSuperHeroes(FinalList);


            int age = 35;

            Superhero returnedhero = FindByAge(age,FinalList);

            String name = "SpiderMan";
            Superhero Super = FindByName(name, FinalList);




            Console.ReadLine();
        }

        private static Superhero FindByName(string name, List<Superhero> finalList)
        {
            
            Superhero temphero1 = new Superhero();
            temphero1=finalList.Select(x => x).Where(x => x.name == name).FirstOrDefault();

           
            if (temphero1 == null)
            {
                Superhero temphero2 = new Superhero();
                temphero2.name = "Batman";
                return temphero2 ;
            }
            else
            return temphero1;
        }

        private static Superhero FindByAge(int age, List<Superhero> finalList)
        {
            Superhero temphero = new Superhero();
            //var temphero = listOfSuperHeroes.Select(x => x).Where(x => x.age == 10);
            temphero = finalList.Select(x => x).Where(x => x.age == age).FirstOrDefault();
            
           
            return temphero;
        }

        private static List<Superhero> GetFiveSuperHeroes()
        {
            var tempList = new List<Superhero>();

            //five super heroes. 
            var name = "SuperMan";
            var power = "Strength";
            var brand = "DC";
            var age = 80;

            //five super heroes. 
            var tempHero = new Superhero();
            var tempHero2 = new Superhero();
            var tempHero3 = new Superhero();
            var tempHero4 = new Superhero();
            var tempHero5 = new Superhero();
            var tempHero6 = new Superhero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 40;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "WonderWoman";
            power = "Compassion";
            brand = "DC";
            age = 35;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "Spiderman";
            power = "Spider";
            brand = "Marvel";
            age = 28;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Ironman";
            power = "Technology";
            brand = "Marvel";
            age = 50;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "DoctorStrange";
            power = "Time";
            brand = "Marvel";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "Hulk";
            power = "Strength";
            brand = "Marvel";
            age = 40;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);


            return tempList;
        }
        private static void ShowSuperHeroes(List<Superhero> listOfSuperHeroes)
        {
            //.OrderByDescending(x => x.Delivery.SubmissionDate);

           listOfSuperHeroes=listOfSuperHeroes.OrderByDescending(x => x.age).ToList();


            foreach (var x in listOfSuperHeroes)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("Name " + x.name);
                Console.WriteLine("Age " + x.age);
                Console.WriteLine("Power  " + x.power);
                Console.WriteLine("Brand " + x.brand);
                Console.WriteLine("------------------");
            }
        }

    }
}